package com.familytree.app;

import com.familytree.app.model.FamilyTree;
import com.familytree.app.processor.CommandProcessor;
import com.familytree.app.seeder.FamilyTreeSeeder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FamilyTreeApplication {

  private final static String DEFAULT_FILE = "/input.txt";

  public static void main(final String[] args) throws Exception {
    var inputStream = createInputStream(DEFAULT_FILE);
    var inputReader = new BufferedReader(new InputStreamReader(inputStream));
    var familyTreeSeeder = new FamilyTreeSeeder(inputReader);
    var familyTree = familyTreeSeeder.seedDataAndBuildFamilyTree();

    inputReader.close();
    findOrUpdateFamilyTree(args, familyTree);
  }

  private static void findOrUpdateFamilyTree(final String[] args, final FamilyTree familyTree) throws Exception {
    InputStream inputStream = System.in;
    if (args.length > 0) {
      inputStream = createInputStream(args[0]);
    }
    var inputReader = new BufferedReader(new InputStreamReader(inputStream));
    var commandProcessor = new CommandProcessor(inputReader);
    commandProcessor.processInput(familyTree);
    inputReader.close();
  }

  private static InputStream createInputStream(final String fileName) throws Exception {
    InputStream inputStream = FamilyTreeApplication.class.getResourceAsStream(fileName);
    if (inputStream == null) {
      inputStream = new FileInputStream(new File(fileName));
    }
    return inputStream;
  }
}
