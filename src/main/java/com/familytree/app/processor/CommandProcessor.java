package com.familytree.app.processor;

import com.familytree.app.model.FamilyTree;
import com.familytree.app.command.Command;
import com.familytree.app.command.CommandFactory;

import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;

public class CommandProcessor {

  private final BufferedReader inputReader;
  private final CommandFactory commandFactory;

  public CommandProcessor(final BufferedReader inputReader) {
    this.inputReader = inputReader;
    this.commandFactory = CommandFactory.getInstance();
  }

  public void processInput(final FamilyTree familyTree) {
    inputReader.lines().takeWhile(line -> !"Done".equalsIgnoreCase(line))
            .map(line -> line.split(" ")).map(tokens -> Arrays.asList(tokens))
            .forEach(argument -> executeCommand(argument, familyTree));
  }

  private void executeCommand(final List<String> arguments, final FamilyTree tree) {
    Command command = commandFactory.getCommandByAction(arguments.get(0));
    String response = command.execute(tree, arguments);
    System.out.println(response);
  }
}
