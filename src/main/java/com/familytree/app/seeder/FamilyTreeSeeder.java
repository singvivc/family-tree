package com.familytree.app.seeder;

import com.familytree.app.model.Family;
import com.familytree.app.model.Individual;
import com.familytree.app.model.FamilyTree;
import com.familytree.app.model.enums.Gender;

import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;

public class FamilyTreeSeeder {

  private final BufferedReader inputReader;

  public FamilyTreeSeeder(final BufferedReader inputReader) {
    this.inputReader = inputReader;
  }

  public FamilyTree seedDataAndBuildFamilyTree() {
    FamilyTree tree = new FamilyTree();
    inputReader.lines().map(line -> line.split(","))
            .filter(tokens -> tokens.length >= 3).map(tokens -> Arrays.asList(tokens))
            .forEach(entry -> addToFamilyTree(entry, tree));
    return tree;
  }

  private void addToFamilyTree(final List<String> entry, final FamilyTree tree) {
    Gender gender = Gender.getGenderByValue(entry.get(1).trim());
    Individual individual = Individual.builder().name(entry.get(0).trim()).gender(gender)
            .parent(findAndUpdateWithParent(entry, tree)).directChild(true).build();

    Individual spouse = buildPersonFromEntry(entry, gender);
    Family newFamily = Family.builder().child(individual).spouse(spouse).build();
    tree.addMember(newFamily, individual.getParent());
  }

  private Family findAndUpdateWithParent(final List<String> entry, final FamilyTree tree) {
    Family parent = null;
    if (entry.get(2) != null && !entry.get(2).isBlank()) {
      parent = tree.searchFamilyPersonBelongsTo(entry.get(2).trim());
    }
    return parent;
  }

  private Individual buildPersonFromEntry(final List<String> entry, final Gender spouseGender) {
    Individual spouse = null;
    if (entry.size() > 3 && entry.get(3) != null && !entry.get(3).isBlank()) {
      Gender gender = Gender.oppositeGender(spouseGender);
      spouse = Individual.builder().name(entry.get(3).trim()).gender(gender).build();
    }
    return spouse;
  }
}
