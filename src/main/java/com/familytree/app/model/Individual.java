package com.familytree.app.model;

import com.familytree.app.model.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Builder(toBuilder = true)
@Getter
@AllArgsConstructor
public class Individual {

  private final String name;
  private final Gender gender;
  private final Family parent;
  private final boolean directChild;

  public List<Individual> getSiblings() {
    if (this.getParent() != null) {
      return this.getParent().getChildren().stream().map(it -> it.getChild())
              .filter(it -> !this.name.equalsIgnoreCase(it.getName())).collect(toList());
    }
    return Collections.emptyList();
  }

  public Individual getParentByGender(final Gender gender) {
    if (parent == null) return null;
    if (parent.getChild().getGender() == gender) {
      return parent.getChild();
    }
    if (parent.getSpouse() == null) return null;
    if (parent.getSpouse().getGender() == gender) {
      return parent.getSpouse();
    }
    return null;
  }
}
