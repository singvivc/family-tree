package com.familytree.app.model;

import com.familytree.app.model.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Builder
@Getter
@AllArgsConstructor
public class Family {

  private final Individual child;
  private final Individual spouse;
  @Builder.Default()
  private final List<Family> children = new ArrayList<>();

  public Optional<Family> getMemberPersonBelongsTo(final String name) {
    Individual individual = (nonNull(child) && name.equalsIgnoreCase(child.getName())) ? child :
            (nonNull(spouse) && name.equalsIgnoreCase(spouse.getName())) ? spouse : null;
    return Optional.ofNullable(individual == null ? null : this);
  }

  public Individual getIndividualFromFamilyByName(final String name) {
    if (name.equalsIgnoreCase(child.getName()))
      return child;
    return spouse != null ? (name.equalsIgnoreCase(spouse.getName()) ? spouse : null) : null;
  }

  public Individual getIndividualByGender(final Gender gender) {
    if (child.getGender() == gender) {
      return child;
    }
    return spouse;
  }

  public Individual spouseOf(final String personName) {
    if (personName.equalsIgnoreCase(child.getName())) {
      return spouse;
    }
    return child;
  }
}
