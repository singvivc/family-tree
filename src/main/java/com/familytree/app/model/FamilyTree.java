package com.familytree.app.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

@Getter
@NoArgsConstructor
public class FamilyTree {

  private Family root;

  public void addMember(final Family family, final Family to) {
    if (to == null) {
      this.root = family;
      return;
    }
    to.getChildren().add(family);
  }

  public Family searchFamilyPersonBelongsTo(final String personName) {
    if (root == null || personName == null || personName.isBlank()) {
      return null;
    }
    return searchMemberPersonBelongsTo(root, personName);
  }

  private Family searchMemberPersonBelongsTo(final Family family, final String personName) {
    Queue<Family> familyQueue = new LinkedList<>();
    familyQueue.offer(family);
    while (!familyQueue.isEmpty()) {
      Family current = familyQueue.poll();
      Optional<Family> optionalMember = current.getMemberPersonBelongsTo(personName);
      if (optionalMember.isPresent()) {
        return optionalMember.get();
      }
      visitChild(current, familyQueue);
    }
    return null;
  }

  private void visitChild(final Family current, final Queue<Family> familyQueue) {
    current.getChildren().stream().forEach(family -> familyQueue.offer(family));
  }
}
