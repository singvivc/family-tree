package com.familytree.app.model.enums;

import lombok.Getter;

public enum Gender {
  Male ("Male"), Female ("Female");

  @Getter
  private final String value;

  private Gender(final String value) {
    this.value = value;
  }

  public static Gender getGenderByValue(final String value) {
    for (Gender gender : Gender.values()) {
      if (value.equalsIgnoreCase(gender.getValue())) {
        return gender;
      }
    }
    return null;
  }

  public static Gender oppositeGender(final Gender gender) {
    return gender == Male ? Female : Male;
  }
}
