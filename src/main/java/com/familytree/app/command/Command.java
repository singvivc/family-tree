package com.familytree.app.command;

import com.familytree.app.model.FamilyTree;

import java.util.List;

public interface Command {

  public String execute(final FamilyTree tree, final List<String> arguments);
  public String getActionName();
}
