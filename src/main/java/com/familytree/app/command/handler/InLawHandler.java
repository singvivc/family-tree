package com.familytree.app.command.handler;

import com.familytree.app.model.Family;
import com.familytree.app.model.Individual;
import com.familytree.app.model.enums.Gender;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

public class InLawHandler implements RelationHandler {

  private final Gender gender;

  public InLawHandler(final Gender gender) {
    this.gender = gender;
  }

  @Override
  public String handleRelation(final Family family, final String name) {
    Individual spouse = family.spouseOf(name);
    List<Individual> siblings = new ArrayList<>();
    if (spouse != null) {
      siblings.addAll(spouse.getSiblings());
    }
    siblings.addAll(spouseOfSiblings(family, name));
    return buildInLaws(siblings);
  }

  private String buildInLaws(final List<Individual> siblings) {
    StringBuilder inLawsBuilder = new StringBuilder();
    siblings.stream().filter(it -> it.getGender() == gender).forEach(member -> {
      inLawsBuilder.append(member.getName()).append(" ");
    });
    return inLawsBuilder.toString().trim();
  }

  private Collection<Individual> spouseOfSiblings(final Family family, final String name) {
    Individual individual = family.getIndividualFromFamilyByName(name);
    if (individual.getParent() != null) {
      return individual.getParent().getChildren().stream().filter(it -> it.getIndividualFromFamilyByName(name) == null)
              .map(Family::getSpouse).filter(Objects::nonNull).collect(toList());
    }
    return Collections.emptyList();
  }
}
