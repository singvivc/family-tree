package com.familytree.app.command.handler;

import com.familytree.app.model.Family;
import com.familytree.app.model.Individual;

public class SiblingsHandler implements RelationHandler {

  @Override
  public String handleRelation(final Family family, final String name) {
    Individual individual = family.getIndividualFromFamilyByName(name);
    var siblings = individual.getSiblings();
    StringBuilder builder = new StringBuilder();
    siblings.stream().forEach(sibling -> builder.append(sibling.getName()).append(" "));
    return builder.toString().trim();
  }
}
