package com.familytree.app.command.handler;

import com.familytree.app.model.Family;
import com.familytree.app.model.Individual;
import com.familytree.app.model.enums.Gender;

import static java.util.stream.Collectors.toList;

public class UncleAuntRelation implements RelationHandler {

  private final Gender gender;
  private final Gender relationGender;

  public UncleAuntRelation(final Gender gender, final Gender relationGender) {
    this.gender = gender;
    this.relationGender = relationGender;
  }

  @Override
  public String handleRelation(final Family family, final String name) {
    Individual individual = family.getIndividualFromFamilyByName(name);
    StringBuilder auntBuilder = new StringBuilder();
    if (individual.getParent() != null) {
      Individual parent = individual.getParent().getIndividualByGender(gender);
      var siblings = parent.getSiblings().stream().filter(it -> it.getGender() == relationGender).collect(toList());
      siblings.stream().forEach(sibling -> auntBuilder.append(sibling.getName()).append(" "));
    }
    return auntBuilder.toString().trim();
  }
}
