package com.familytree.app.command.handler;

import com.familytree.app.model.Family;
import com.familytree.app.model.Individual;
import com.familytree.app.model.enums.Gender;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class ChildHandler implements RelationHandler {

  private final Gender gender;

  public ChildHandler(final Gender gender) {
    this.gender = gender;
  }

  @Override
  public String handleRelation(final Family family, final String name) {
    List<Individual> children = family.getChildren().stream().map(Family::getChild).collect(toList());
    StringBuilder childBuilder = new StringBuilder();
    children.stream().filter(it -> this.gender == it.getGender())
            .forEach(child -> childBuilder.append(child.getName()).append(" "));
    return childBuilder.toString().trim();
  }
}
