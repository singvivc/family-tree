package com.familytree.app.command.handler;

import com.familytree.app.model.Family;

public interface RelationHandler {

  String handleRelation(final Family family, final String name);
}
