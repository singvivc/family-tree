package com.familytree.app.command.handler.enums;

import com.familytree.app.command.exception.InvalidRelationHandlerException;
import com.familytree.app.command.handler.UncleAuntRelation;
import com.familytree.app.command.handler.ChildHandler;
import com.familytree.app.command.handler.InLawHandler;
import com.familytree.app.command.handler.RelationHandler;
import com.familytree.app.command.handler.SiblingsHandler;

import java.util.Arrays;
import java.util.Optional;

import static com.familytree.app.model.enums.Gender.Female;
import static com.familytree.app.model.enums.Gender.Male;

public enum Handlers {

  SIBLINGS ("Siblings", new SiblingsHandler()),
  SON ("Son", new ChildHandler(Male)),
  DAUGHTER ("Daughter", new ChildHandler(Female)),
  BROTHER_IN_LAWS ("Brother-In-Law", new InLawHandler(Male)),
  SISTER_IN_LAWS ("Sister-In-Law", new InLawHandler(Female)),
  MATERNAL_AUNT ("Maternal-Aunt", new UncleAuntRelation(Female, Female)),
  PATERNAL_AUNT ("Paternal-Aunt", new UncleAuntRelation(Male, Female)),
  MATERNAL_UNCLE ("Maternal-Uncle", new UncleAuntRelation(Female, Male)),
  PATERNAL_UNCLE ("Paternal-Uncle", new UncleAuntRelation(Male, Male));

  private final String value;
  private final RelationHandler relationHandler;

  private Handlers(final String value, final RelationHandler relationHandler) {
    this.value = value;
    this.relationHandler = relationHandler;
  }

  public String getValue() {
    return this.value;
  }

  public RelationHandler getRelationHandler() {
    return this.relationHandler;
  }

  public static RelationHandler getRelationHandlerByValue(final String value) {
    Optional<Handlers> optionalHandler = Arrays.asList(Handlers.values()).stream()
            .filter(handler -> value.equalsIgnoreCase(handler.getValue())).findFirst();
    Handlers handler = optionalHandler.orElseThrow(
            () -> new InvalidRelationHandlerException("May not be implemented yet"));
    return handler.getRelationHandler();
  }
}
