package com.familytree.app.command;

import com.familytree.app.model.FamilyTree;
import com.familytree.app.command.exception.InvalidActionNameException;

import java.util.List;

public class NoOps implements Command {

  @Override
  public String execute(final FamilyTree tree, final List<String> arguments) {
    throw new InvalidActionNameException("Invalid action name exception");
  }

  @Override
  public String getActionName() {
    return null;
  }
}
