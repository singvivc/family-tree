package com.familytree.app.command;

import com.familytree.app.model.Family;
import com.familytree.app.model.FamilyTree;
import com.familytree.app.model.Individual;

import java.util.List;

import static com.familytree.app.command.Constants.CHILD_ADDITION_FAILED;
import static com.familytree.app.command.Constants.CHILD_ADDITION_SUCCEEDED;
import static com.familytree.app.command.Constants.PERSON_NOT_FOUND;
import static com.familytree.app.model.enums.Gender.Female;
import static com.familytree.app.model.enums.Gender.getGenderByValue;

public class AddChild implements Command {

  @Override
  public String execute(final FamilyTree tree, final List<String> arguments) {
    final String motherName = arguments.get(1);
    Family family = tree.searchFamilyPersonBelongsTo(motherName);
    if (family == null) return PERSON_NOT_FOUND;

    Individual individual = family.getIndividualFromFamilyByName(arguments.get(1));
    if (individual.getGender() != Female) return CHILD_ADDITION_FAILED;
    var child = Individual.builder().name(arguments.get(2)).gender(getGenderByValue(arguments.get(3)))
            .parent(family).build();
    Family newFamily = Family.builder().child(child).build();
    family.getChildren().add(newFamily);
    return CHILD_ADDITION_SUCCEEDED;
  }

  @Override
  public String getActionName() {
    return "ADD_CHILD";
  }
}
