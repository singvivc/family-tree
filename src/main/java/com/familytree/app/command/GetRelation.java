package com.familytree.app.command;

import com.familytree.app.command.handler.enums.Handlers;
import com.familytree.app.model.Family;
import com.familytree.app.model.FamilyTree;

import java.util.List;

import static com.familytree.app.command.Constants.NONE;
import static com.familytree.app.command.Constants.PERSON_NOT_FOUND;

public class GetRelation implements Command {

  @Override
  public String execute(final FamilyTree tree, final List<String> arguments) {
    Family family = tree.searchFamilyPersonBelongsTo(arguments.get(1));
    if (family == null) {
      return PERSON_NOT_FOUND;
    }
    var relationHandler = Handlers.getRelationHandlerByValue(arguments.get(2));
    var response = relationHandler.handleRelation(family, arguments.get(1));
    return response.length() > 0 ? response : NONE;
  }

  @Override
  public String getActionName() {
    return "GET_RELATIONSHIP";
  }
}
