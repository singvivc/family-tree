package com.familytree.app.command.exception;

public class InvalidRelationHandlerException extends RuntimeException {

  public InvalidRelationHandlerException() {

  }

  public InvalidRelationHandlerException(final String errorMessage) {
    super(errorMessage);
  }
}
