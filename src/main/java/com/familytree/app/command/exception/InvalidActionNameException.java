package com.familytree.app.command.exception;

public class InvalidActionNameException extends RuntimeException {

  public InvalidActionNameException(final String errorMessage) {
    super(errorMessage);
  }
}
