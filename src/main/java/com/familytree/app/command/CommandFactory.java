package com.familytree.app.command;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public final class CommandFactory {

  private static final CommandFactory INSTANCE = new CommandFactory();
  private final Map<String, Command> actionByCommand = new HashMap<>();

  private CommandFactory() {
    autoCreateCommandBean(actionByCommand);
  }

  public static CommandFactory getInstance() {
    return INSTANCE;
  }

  public Command getCommandByAction(final String actionName) {
    String action = !actionName.isBlank() ? actionName.trim().toUpperCase() : "";
    return actionByCommand.getOrDefault(action, new NoOps());
  }

  private void autoCreateCommandBean(final Map<String, Command> actionByCommand) {
    ServiceLoader<Command> commandServiceLoader = ServiceLoader.<Command>load(Command.class);
    commandServiceLoader.forEach(command -> actionByCommand.put(command.getActionName(), command));
  }
}
