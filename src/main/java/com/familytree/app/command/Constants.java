package com.familytree.app.command;

public final class Constants {
  private Constants() {}

  public static final String PERSON_NOT_FOUND = "PERSON_NOT_FOUND";
  public static final String CHILD_ADDITION_FAILED = "CHILD_ADDITION_FAILED";
  public static final String CHILD_ADDITION_SUCCEEDED = "CHILD_ADDITION_SUCCEEDED";

  public static final String NONE = "NONE";
}
