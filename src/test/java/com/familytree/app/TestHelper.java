package com.familytree.app;

import com.familytree.app.model.FamilyTree;
import com.familytree.app.seeder.FamilyTreeSeeder;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.util.stream.Stream;

public final class TestHelper {

  private TestHelper() {}

  public static FamilyTree generateFamilyTree(final BufferedReader inputReader) {
    Mockito.when(inputReader.lines()).thenReturn(Stream.of(defaultInput()));
    FamilyTreeSeeder treeSeeder = new FamilyTreeSeeder(inputReader);
    return treeSeeder.seedDataAndBuildFamilyTree();
  }

  private static String[] defaultInput() {
    return new String[] {"Shan, Male, , Anga", "Chit, Male, Anga, Amba", "Ish, Male, Anga,",
            "Vich, Male, Anga, Lika", "Aras, Male, Anga, Chitra", "Satya, Female, Anga, Vyan",
            "Dritha, Female, Amba, Jaya", "Tritha, Female, Amba,", "Vritha, Male, Amba,",
            "Vila, Female, Lika,", "Chika, Female, Lika,", "Jnki, Female, Chitra, Arit", "Ahit, Male, Chitra,",
            "Asva, Male, Satya, Satvy", "Vyas, Male, Satya, Krpi", "Atya, Female, Satya,", "Yodhan, Male, Dritha,",
            "Laki, Male, Jnki,", "Lavnya, Female, Jnki,", "Vasa, Male, Satvy,", "Kriya, Male, Krpi,",
            "Krithi, Female, Krpi,"};
  }
}
