package com.familytree.app.seeder;

import com.familytree.app.model.enums.Gender;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FamilyTreeSeederTest {

  private static BufferedReader bufferedReader;
  private static FamilyTreeSeeder familyTreeSeeder;

  @BeforeClass
  public static void beforeClass() {
    bufferedReader = mock(BufferedReader.class);
    familyTreeSeeder = new FamilyTreeSeeder(bufferedReader);
  }

  @Test
  public void givenValidTreeInput_testShouldGenerateValidTree() {
    var input = Stream.of("King Shan, Male, , Queen Anga", "Chit, Male, Queen Anga, Amba", "Ish, Male, Queen Anga,");
    when(bufferedReader.lines()).thenReturn(input);

    var familyTree = familyTreeSeeder.seedDataAndBuildFamilyTree();
    assertThat(familyTree, notNullValue());
    assertThat(familyTree.getRoot().getChild().getName(), equalTo("King Shan"));
    assertThat(familyTree.getRoot().getChild().getGender(), is(Gender.Male));

    assertThat(familyTree.getRoot().getSpouse().getName(), equalTo("Queen Anga"));
    assertThat(familyTree.getRoot().getSpouse().getGender(), is(Gender.Female));
    assertThat(familyTree.getRoot().getChildren().size(), is(2));
  }
}
