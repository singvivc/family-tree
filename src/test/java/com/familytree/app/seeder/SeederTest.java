package com.familytree.app.seeder;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.util.stream.Stream;

import static com.familytree.app.model.enums.Gender.Female;
import static com.familytree.app.model.enums.Gender.Male;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SeederTest {

  private static BufferedReader inputReader;
  //private static Seeder seeder;

  @BeforeClass
  public static void beforeClass() {
    inputReader = mock(BufferedReader.class);
    //seeder = new Seeder(inputReader);
  }

 /* @Test
  public void givenSeedData_testShouldGenerateFamilyTree() {
    Stream<String> lines = Stream.of("King Shan, Male, , Queen Anga");
    when(inputReader.lines()).thenReturn(lines);

    FamilyTree tree = seeder.seedData();
    assertThat(tree, notNullValue());
    assertThat(tree.getRoot(), notNullValue());

    var individual = tree.getRoot();
    assertThat(individual.getName(), equalTo("King Shan"));
    assertThat(individual.getGender(), is(Male));
    assertThat(individual.getSpouse().get().getName(), equalTo("Queen Anga"));
    assertThat(individual.getSpouse().get().getGender(), is(Female));
    assertThat(individual.getChildren().size(), is(0));
  }

  @Test
  public void givenSeedData_testShouldGenerateFamilyTreeWithChildren() {
    Stream<String> lines = Stream.of("King Shan, Male, , Queen Anga", "Chit, Male, Queen Anga, Amba",
            "Ish, Male, Queen Anga,", "Vich, Male, Queen Anga, Lika");
    when(inputReader.lines()).thenReturn(lines);

    FamilyTree tree = seeder.seedData();
    assertThat(tree, notNullValue());
    assertThat(tree.getRoot().getChildren().size(), is(3));
  }*/
}
