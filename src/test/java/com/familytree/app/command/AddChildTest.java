package com.familytree.app.command;

import com.familytree.app.TestHelper;
import com.familytree.app.model.Family;
import com.familytree.app.model.FamilyTree;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(MockitoExtension.class)
public class AddChildTest {

  private static BufferedReader inputReader;

  @BeforeClass
  public static void setUp() {
    inputReader = Mockito.mock(BufferedReader.class);
  }

  @Test
  public void givenFamilyTree_testShouldAddNewFamilyToTheTree() {
    FamilyTree familyTree = TestHelper.generateFamilyTree(inputReader);
    List<String> arguments = List.of("ADD_CHILD", "Chitra", "Aria", "Female");

    AddChild addChild = new AddChild();
    final String response = addChild.execute(familyTree, arguments);
    assertThat(response, equalTo("CHILD_ADDITION_SUCCEEDED"));
    Family familyPersonBelongsTo = familyTree.searchFamilyPersonBelongsTo("Aria");
    assertThat(familyPersonBelongsTo, notNullValue());
    assertThat(familyPersonBelongsTo.getIndividualFromFamilyByName("Aria"), notNullValue());
  }

  @Test
  public void givenFamilyTree_testShouldNotSucceedInAddingNewChildToANonExistingMember() {
    FamilyTree familyTree = TestHelper.generateFamilyTree(inputReader);
    List<String> arguments = List.of("ADD_CHILD", "Vichitra", "Aria", "Female");

    AddChild addChild = new AddChild();
    final String response = addChild.execute(familyTree, arguments);
    assertThat(response, equalTo("PERSON_NOT_FOUND"));
    Family familyPersonBelongsTo = familyTree.searchFamilyPersonBelongsTo("Aria");
    assertThat(familyPersonBelongsTo, nullValue());
  }

  @Test
  public void givenFamilyTree_testShouldNotAddChildToAMaleMember() {
    FamilyTree familyTree = TestHelper.generateFamilyTree(inputReader);
    List<String> arguments = List.of("ADD_CHILD", "Asva", "Vani", "Female");

    AddChild addChild = new AddChild();
    final String response = addChild.execute(familyTree, arguments);
    assertThat(response, equalTo("CHILD_ADDITION_FAILED"));
  }
}
