package com.familytree.app.command;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

public class CommandFactoryTest {

  private static CommandFactory commandFactory;

  @BeforeClass
  public static void beforeClass() {
    commandFactory = CommandFactory.getInstance();
  }

  @Test
  public void testShouldReturnAddChildCommandForAddChildActionName() {
    Command command = this.commandFactory.getCommandByAction("ADD_CHILD");
    assertThat(command, instanceOf(AddChild.class));
  }

  @Test
  public void testShouldReturnAddChildCommandForAddChildActionNameInLowerCase() {
    Command command = this.commandFactory.getCommandByAction("add_child");
    assertThat(command, instanceOf(AddChild.class));
  }

  @Test
  public void testShouldReturnNoOpsCommandForUnknownActionName() {
    Command command = commandFactory.getCommandByAction("SOME_COMMAND");
    assertThat(command, instanceOf(NoOps.class));
  }
}
