package com.familytree.app.command;

import com.familytree.app.TestHelper;
import com.familytree.app.model.FamilyTree;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

@RunWith(value = Parameterized.class)
public class GetRelationTest {

  private static BufferedReader inputReader;
  private static FamilyTree familyTree;
  private final String person;
  private final String relationType;
  private final List<String> expectedAnswer;

  public GetRelationTest(final String relationType, final String person, final List<String> expectedAnswer) {
    this.relationType = relationType;
    this.person = person;
    this.expectedAnswer = expectedAnswer;
  }

  @BeforeClass
  public static void setUp() {
    inputReader = Mockito.mock(BufferedReader.class);
    familyTree = TestHelper.generateFamilyTree(inputReader);
  }

  @Parameterized.Parameters(name = "{0} of {1} should be {2}")
  public static Iterable<Object[]> data() {
    return Arrays.asList(new Object[][] {
            {"Son", "Shan", List.of("Chit", "Ish", "Vich", "Aras")},
            {"Son", "Jaya", List.of("Yodhan")},
            {"Son", "Chit", List.of("Vritha")},
            {"Son", "Satya", List.of("Asva", "Vyas")},
            {"Daughter", "Satya", List.of("Atya")},
            {"Daughter", "Anga", List.of("Satya")},
            {"Daughter", "Aras", List.of("Jnki")},
            {"Daughter", "Lika", List.of("Vila", "Chika")},
            {"Brother-In-Law", "Satya", List.of("NONE")},
            {"Brother-In-Law", "Jaya", List.of("Vritha")},
            {"Brother-In-Law", "Satvy", List.of("Vyas")},
            {"Brother-In-Law", "Amba", List.of("Ish", "Vich", "Aras")},
            {"Sister-In-Law", "Chitra", List.of("Satya")},
            {"Sister-In-Law", "Jaya", List.of("Tritha")},
            {"Sister-In-Law", "Satvy", List.of("Atya")},
            {"Sister-In-Law", "Amba", List.of("Satya")},
            {"Sister-In-Law", "Atya", List.of("Satvy", "Krpi")},
            {"Maternal-Aunt", "Yodhan", List.of("Tritha")},
            {"Paternal-Aunt", "Dritha", List.of("Satya")},
            {"Paternal-Aunt", "Vasa", List.of("Atya")},
            {"Paternal-Aunt", "Krithi", List.of("Atya")},
            {"Maternal-Uncle", "Dritha", List.of("NONE")},
            {"Maternal-Uncle", "Asva", List.of("Chit", "Ish", "Vich", "Aras")},
            {"Paternal-Uncle", "Dritha", List.of("Ish", "Vich", "Aras")},
            {"Paternal-Uncle", "Jnki", List.of("Chit", "Ish", "Vich")},
            {"Paternal-Uncle", "Kriya", List.of("Asva")},
            {"Siblings", "Chit", List.of("Ish", "Vich", "Aras", "Satya")},
            {"Siblings", "Amba", List.of("NONE")},
            {"Siblings", "Dritha", List.of("Tritha", "Vritha")},
            {"Siblings", "Vila", List.of("Chika")},
            {"Siblings", "Jnki", List.of("Ahit")},
            {"Siblings", "Asva", List.of("Vyas", "Atya")},
            {"Siblings", "Yodhan", List.of("NONE")},
            {"Siblings", "Laki", List.of("Lavnya")},
            {"Siblings", "Vasa", List.of("NONE")},
            {"Siblings", "Kriya", List.of("Krithi")},
    });
  }

  @Test
  public void givenFamilyTreeAndAName_testShouldReturnTheMaleChildOfThatPerson() {
    GetRelation getRelation = new GetRelation();

    var relativeNames = getRelation.execute(familyTree, List.of("GET_RELATIONSHIP", person, relationType));
    assertThat(relativeNames, notNullValue());

    List<String> response = Arrays.asList(relativeNames.split(" "));
    assertThat(response.size(), is(expectedAnswer.size()));
    assertThat(response, containsInAnyOrder(expectedAnswer.toArray()));
  }
}
