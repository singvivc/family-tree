package com.familytree.app.processor;

import com.familytree.app.model.Family;
import com.familytree.app.model.FamilyTree;
import com.familytree.app.model.Individual;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.util.stream.Stream;

import static com.familytree.app.model.enums.Gender.Female;
import static com.familytree.app.model.enums.Gender.Male;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CommandProcessorTest {

  private static BufferedReader inputReader;
  private static CommandProcessor commandProcessor;

  @BeforeClass
  public static void beforeEach() {
    inputReader = mock(BufferedReader.class);
    commandProcessor = new CommandProcessor(inputReader);
  }

  @Test
  public void givenAFamilyTree_testShouldExecuteTheCommandFromInput() {
    FamilyTree familyTree = defaultFamilyTree();
    when(inputReader.lines()).thenReturn(Stream.of("ADD_CHILD Anga Ish Male"));
    commandProcessor.processInput(familyTree);

    assertThat(familyTree.getRoot().getChildren().size(), is(2));
    var family = familyTree.searchFamilyPersonBelongsTo("Ish");
    assertThat(family, notNullValue());
  }

  @Test
  public void givenAFamilyTree_testShouldExecuteAddChildCommandToAddChildToUnknownMember() {
    FamilyTree familyTree = defaultFamilyTree();
    assertThat(familyTree.getRoot().getChildren().size(), is(1));

    when(inputReader.lines()).thenReturn(Stream.of("ADD_CHILD Bunga Ish Male"));
    commandProcessor.processInput(familyTree);

    assertThat(familyTree.getRoot().getChildren().size(), is(1));
    var family = familyTree.searchFamilyPersonBelongsTo("Ish");
    assertThat(family, nullValue());
  }

  private FamilyTree defaultFamilyTree() {
    Family shanFamily = Family.builder().child(Individual.builder().name("Shan").gender(Male).build())
            .spouse(Individual.builder().name("Anga").gender(Female).build()).build();
    FamilyTree tree = new FamilyTree();
    Family chitFamily = Family.builder().child(Individual.builder().name("Chit").gender(Male).build())
            .spouse(Individual.builder().name("Amba").gender(Female).build()).build();

    tree.addMember(shanFamily, null);
    tree.addMember(chitFamily, shanFamily);
    return tree;
  }
}
