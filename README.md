### Meet The Family

#### Table of Contents
   * [System Requirements](#system-requirements)
   * [How to use](#how-to-use)
   * [Assumptions](#assumptions)
   * [Description](#description)
   * [Commands and Description](#commands-and-description)

#### System Requirements
The minimum requirement to run the application is to have Java version 12 or above. In order to build the application it is mandatory to have Maven version 3.0.0 or above.

#### How to use
In order to build the application execute the following command

```
mvn clean install
```

To run the application, use the following command on the terminal

```	
java -jar path_to_binary/geektrust.jar
```

path\_to\_binary is the location of the binary file **<geektrust.jar>**

#### Assumptions
There were few assumptions made while writing the applications.

1. The names in the family tree should not have space in between.

2. However, multiple individuals can have the same names in the family tree, but while making query for the same name the first node encountered will be returned to the output. So, it is assumed that the user will provide only unique name while building or updating the tree.

#### Description
**Meet the family** is a problem where given an already constructed family hierarchy or the new one, one can 
query or update the given hierarchy. The application currently supports the following commands to query or update
the family tree.
  
   * **ADD_CHILD**  <pre>  (_Add a new Child to any family through a mother_)
   * **GET_RELATIONSHIP** <pre>  (_Query for the given Relationship in any of the family_)
   
#### Commands and Description

**ADD_CHILD**: Create a new child in the family tree. The child can be added to a tree only through mother. When an attempt is made to add a child to a mother who doesn't exists in 
the tree then the application will return with error message **PERSON_NOT_FOUND**. However, when an attempt is made to add a child through any other than mother then the 
application will return with error message **CHILD_ADDITION_FAILED**. On successful addition of child the application will return with the message **CHILD_ADDITION_SUCCEEDED**. 
The command expects to be run in the format given below.

```
ADD_CHILD <MOTHER_NAME> <NAME_OF_CHILD> <GENDER_OF_CHILD>
Example
ADD_CHILD Anga Chit male
```

**GET_RELATIONSHIP**: Will be used to query the given family hierarchy. The application will react to this command based on the presence or absence of the name in the family tree. When the name looking for does not exist in the family tree then the application will return **PERSON_NOT_FOUND**, when the name is found but the relationship looking for doesn't exist then the application will return **NONE** and when the name is found and the relationship looking for also exist then the application will return with the list of option each separated by comma. The syntax of the command is shown below

```
GET_RELATIONSHIP <NAME_OF_THE_PERSON> <RELATION_SHIP>
Where <RELATION_SHIP> can be any one from the below list of option
	1. Paternal-Uncle
	2. Maternal-Uncle
	3. Paternal-Aunt
	4. Maternal-Aunt
	5. Sister-In-Law
	6. Brother-In-Law
	7. Son
	8. Daughter
	9. Siblings
```
